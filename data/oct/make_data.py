import os 
import argparse 
import random 
from glob import glob
import numpy as np 
from numpy.random import *
seed = 111
import cv2
from tqdm import tqdm

parser = argparse.ArgumentParser()
parser.add_argument('--image_dir', type=str, default='/root/workspace/dataset/OCT')
parser.add_argument('--im_size', type=int)
args = parser.parse_args()

file_path_list = glob(args.image_dir + '/*/*/*.jpeg', recursive=True)
target_size = (args.im_size, args.im_size)

X = []
Y = []

for file_path in tqdm(file_path_list):
    img = cv2.imread(file_path, cv2.IMREAD_GRAYSCALE)

    # 2値化
    _, th_img = cv2.threshold(img, 250, 255, cv2.THRESH_BINARY)

    # 白の輪郭を取り出す
    _, contours, _ = cv2.findContours(th_img, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    img_ok = None
    if len(contours) == 0:
        img_ok = img

    # 余白が画像の縁に隣接していたら、その画像は使わない。
    else:
        max_area = 0
        max_area_index = -1
        for i, cnt in enumerate(contours):
            area = cv2.contourArea(cnt)
            if area > max_area:
                max_area = area 
                max_area_index = i
        
        # 隣接チェック
        check_list = [int(0), int(img.shape[0]-1), int(img.shape[1]-1)]
        check_flag = True 
        for check_i in check_list:
            if (max_area > 1) and (max_area_index != -1):
                if check_i in contours[max_area_index]:
                    check_flag = False
                    break
        
        if check_flag:
            img_ok = img
    
    if not img_ok is None:
        resized_img = cv2.resize(img_ok, target_size)
        X.append(resized_img)

        if 'CNV' in file_path:
            Y.append([1.0, 0.0, 0.0, 0.0])
        
        if 'DME' in file_path:
            Y.append([0.0, 1.0, 0.0, 0.0])

        if 'DRUSEN' in file_path:
            Y.append([0.0, 0.0, 1.0, 0.0])

        if 'NORMAL' in file_path:
            Y.append([0.0, 0.0, 0.0, 1.0])

X = np.asarray(X)
Y = np.asarray(Y)
X = np.reshape(X, (X.shape[0], X.shape[1], X.shape[2], 1))

num_class = Y.shape[1]

Idx = list( range(0, X.shape[0]) )
np.random.seed(seed)
IdxRand = np.random.permutation( Idx )
X, Y = X[IdxRand], Y[IdxRand]

per7 = int(X.shape[0] * 0.7)
X_train, Y_train = X[:per7], Y[:per7]
X_test, Y_test = X[per7:], Y[per7:]

for class_i in range(num_class):
    np.random.seed(111)
    p = np.random.choice( np.where(Y_train[:, class_i]==1)[0], 1960, replace=False )
    if class_i == 0:
        trainIdx = p
    else:
        trainIdx = np.concatenate([trainIdx, p])
trainIdx = np.random.permutation(trainIdx)
X_train, Y_train = X_train[trainIdx], Y_train[trainIdx]

for class_i in range(num_class):
    np.random.seed(111)
    p = np.random.choice( np.where(Y_test[:, class_i]==1)[0], 840, replace=False )
    if class_i == 0:
        testIdx = p
    else:
        testIdx = np.concatenate([testIdx, p])
testIdx = np.random.permutation(testIdx)
X_test, Y_test = X_test[testIdx], Y_test[testIdx]

X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
Y_train = Y_train.astype('float32')
Y_test = Y_test.astype('float32')

print(X_train.shape, Y_train.shape)
print(X_test.shape, Y_test.shape)
print(X_train.dtype, Y_train.dtype)
print(X_test.dtype, Y_test.dtype)

np.save('X_train_{}x{}.npy'.format(args.im_size, args.im_size), X_train)
np.save('X_test_{}x{}.npy'.format(args.im_size, args.im_size), X_test)
np.save('Y_train_{}x{}.npy'.format(args.im_size, args.im_size), Y_train)
np.save('Y_test_{}x{}.npy'.format(args.im_size, args.im_size), Y_test)
