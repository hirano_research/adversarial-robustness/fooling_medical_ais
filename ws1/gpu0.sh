# size
for eps in 0.06 0.05 0.04 0.03 0.02; do
    python make_noise.py \
        --dataset chestx \
        --nlz 11 \
        --model_type inceptionv3 \
        --im_size 224 \
        --eps ${eps} \
        --gpu 0
done

for eps in 0.06 0.05 0.04 0.03 0.02; do
    python make_noise.py \
        --dataset chestx \
        --nlz 11 \
        --model_type inceptionv3 \
        --im_size 150 \
        --eps ${eps} \
        --gpu 0
done

# nlz
for eps in 0.06 0.05 0.04 0.03 0.02; do
    python make_noise.py \
        --dataset chestx \
        --nlz 01 \
        --model_type inceptionv3 \
        --im_size 299 \
        --eps ${eps} \
        --gpu 0
done

# model
for eps in 0.06 0.05 0.04 0.03 0.02; do
    python make_noise.py \
        --dataset chestx \
        --nlz 11 \
        --model_type vgg16 \
        --im_size 299 \
        --eps ${eps} \
        --gpu 0
done

for eps in 0.06 0.05 0.04 0.03 0.02; do
    python make_noise.py \
        --dataset chestx \
        --nlz 11 \
        --model_type vgg19 \
        --im_size 299 \
        --eps ${eps} \
        --gpu 0
done

for eps in 0.06 0.05 0.04 0.03 0.02; do
    python make_noise.py \
        --dataset chestx \
        --nlz 11 \
        --model_type resnet50 \
        --im_size 299 \
        --eps ${eps} \
        --gpu 0
done