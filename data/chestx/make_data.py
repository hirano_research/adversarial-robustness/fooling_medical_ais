import os 
import argparse 
import random 
from glob import glob
import numpy as np
from numpy.random import *
seed = 111
from keras.preprocessing.image import img_to_array, load_img
from tqdm import tqdm

parser = argparse.ArgumentParser()
parser.add_argument('--image_dir', type=str, default='/root/workspace/dataset/chest_xray')
parser.add_argument('--im_size', type=int)
args = parser.parse_args()

file_path_list = glob(args.image_dir + '/*/*/*.jpeg', recursive=True)
target_size = (args.im_size, args.im_size)

X = []
Y = []

for file_path in tqdm(file_path_list):
    img = img_to_array(load_img(file_path, grayscale=True, color_mode='grayscale', target_size=target_size))
    X.append(img)

    if 'NORMAL' in file_path:
        Y.append([1.0, 0.0])

    if 'PNEUMONIA' in file_path:
        Y.append([0.0, 1.0])

X = np.asarray(X)
Y = np.asarray(Y)

num_class = Y.shape[1]

Idx = list( range(0, X.shape[0]) )
np.random.seed(seed)
IdxRand = np.random.permutation( Idx )
X, Y = X[IdxRand], Y[IdxRand]

per7 = int(X.shape[0] * 0.7)
X_train, Y_train = X[:per7], Y[:per7]
X_test, Y_test = X[per7:], Y[per7:]

for class_i in range(num_class):
    np.random.seed(111)
    p = np.random.choice( np.where(Y_train[:, class_i]==1)[0], 900, replace=False )
    if class_i == 0:
        trainIdx = p
    else:
        trainIdx = np.concatenate([trainIdx, p])
trainIdx = np.random.permutation(trainIdx)
X_train, Y_train = X_train[trainIdx], Y_train[trainIdx]

for class_i in range(num_class):
    np.random.seed(111)
    p = np.random.choice( np.where(Y_test[:, class_i]==1)[0], 270, replace=False )
    if class_i == 0:
        testIdx = p
    else:
        testIdx = np.concatenate([testIdx, p])
testIdx = np.random.permutation(testIdx)
X_test, Y_test = X_test[testIdx], Y_test[testIdx]

X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
Y_train = Y_train.astype('float32')
Y_test = Y_test.astype('float32')

print(X_train.shape, Y_train.shape)
print(X_test.shape, Y_test.shape)
print(X_train.dtype, Y_train.dtype)
print(X_test.dtype, Y_test.dtype)

np.save('X_train_{}x{}.npy'.format(args.im_size, args.im_size), X_train)
np.save('X_test_{}x{}.npy'.format(args.im_size, args.im_size), X_test)
np.save('Y_train_{}x{}.npy'.format(args.im_size, args.im_size), Y_train)
np.save('Y_test_{}x{}.npy'.format(args.im_size, args.im_size), Y_test)
