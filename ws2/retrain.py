import argparse
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument('--dataset', type=str, default='chestx')
parser.add_argument('--nlz', type=str, default='01') # 11
parser.add_argument('--model_type', type=str, default='inceptionv3')
parser.add_argument('--im_size', type=int)
parser.add_argument('--target', type=int, default=99)
parser.add_argument('--eps', type=float)
parser.add_argument('--norm', type=str, default='2')
parser.add_argument('--retrain_type', type=str, default='plus')
parser.add_argument('--gpu', type=str)
args = parser.parse_args()
print(args)

import tensorflow as tf
import keras
from keras.backend.tensorflow_backend import set_session
config = tf.ConfigProto(
    gpu_options=tf.GPUOptions(
        visible_device_list=args.gpu, # specify GPU number
        allow_growth=True
    )
)
set_session(tf.Session(config=config))

from keras.applications.inception_v3 import InceptionV3
from keras.applications.vgg16 import VGG16
from keras.applications.vgg19 import VGG19
from keras.applications.resnet50 import ResNet50
from keras.layers import Lambda, Input, Dense, GlobalAveragePooling2D
from keras.models import Model
from keras.callbacks import ModelCheckpoint
from keras.optimizers import SGD

from keras.utils.data_utils import Sequence
from imblearn.over_sampling import RandomOverSampler
from imblearn.keras import balanced_batch_generator
from keras.preprocessing.image import ImageDataGenerator

def get_preds(classifier, X, Y):
    preds = np.argmax(classifier.predict(X), axis=1)
    acc = np.sum(preds == np.argmax(Y, axis=1)) / Y.shape[0]
    return acc, preds

def get_fr(preds_adv, preds):
    fooling_adv = np.sum(preds_adv != preds) / preds.shape[0]
    return fooling_adv

# load data
X_train = np.load('../data/{}/X_train_{}x{}.npy'.format(args.dataset, args.im_size, args.im_size))
X_test = np.load('../data/{}/X_test_{}x{}.npy'.format(args.dataset, args.im_size, args.im_size))
Y_train = np.load('../data/{}/Y_train_{}x{}.npy'.format(args.dataset, args.im_size, args.im_size))
Y_test = np.load('../data/{}/Y_test_{}x{}.npy'.format(args.dataset, args.im_size, args.im_size))
#X_train, X_test, Y_train, Y_test = X_train[:65], X_test[:65], Y_train[:65], Y_test[:65]
if args.nlz == '01':
    print('nlz: 01')
    X_train /= 255.0
    X_test /= 255.0
elif args.nlz == '11':
    print('nlz: 11')
    X_train -= 128.0
    X_test -= 128.0
    X_train /= 128.0
    X_test /= 128.0

noise = np.load('../ws1/noise/{}_{}_{}_{}x{}_{}_{}_{}.npy'.format(args.dataset, args.nlz, args.model_type, args.im_size, args.im_size, args.target, args.eps, args.norm))
X_train_adv = X_train + noise
X_test_adv = X_test + noise

X_train_plus = np.concatenate([X_train, X_train_adv])
X_test_plus = np.concatenate([X_test, X_test_adv])
Y_train_plus = np.concatenate([Y_train, Y_train])
Y_test_plus = np.concatenate([Y_test, Y_test])

if args.retrain_type == 'plus':
    print('re train data: x + x_adv !!!')
    X_re_train = X_train_plus
    X_re_test = X_test_plus
    Y_re_train = Y_train_plus
    Y_re_test = Y_test_plus
elif args.retrain_type == 'only':
    print('re train data: x_adv !!!')
    X_re_train = X_train_adv
    X_re_test = X_test_adv
    Y_re_train = Y_train
    Y_re_test = Y_test

if args.dataset == 'melanoma':
    if args.model_type == 'inceptionv3':
        base_model = InceptionV3(weights='imagenet', input_shape=(args.im_size, args.im_size, 3), include_top=False)
    elif args.model_type == 'vgg16':
        base_model = VGG16(weights='imagenet', input_shape=(args.im_size, args.im_size, 3), include_top=False)
    elif args.model_type == 'vgg19':
        base_model = VGG19(weights='imagenet', input_shape=(args.im_size, args.im_size, 3), include_top=False)
    elif args.model_type == 'resnet50':
        base_model = ResNet50(weights='imagenet', input_shape=(args.im_size, args.im_size, 3), include_top=False)
    x = base_model.output
    x = GlobalAveragePooling2D()(x)
    predictions = Dense(Y_train.shape[1], activation='softmax')(x)
    model = Model(inputs=base_model.input, outputs=predictions)
else:
    # 入力層を変更
    if args.model_type == 'inceptionv3':
        base_model = InceptionV3(weights='imagenet', include_top=False)
    elif args.model_type == 'vgg16':
        base_model = VGG16(weights='imagenet', include_top=False)
    elif args.model_type == 'vgg19':
        base_model = VGG19(weights='imagenet', include_top=False)
    elif args.model_type == 'resnet50':
        base_model = ResNet50(weights='imagenet', include_top=False)
    base_model.layers.pop(0) # remove input layer
    newInput = Input(batch_shape=(None, args.im_size,args.im_size,1))
    x = Lambda(lambda image: tf.image.grayscale_to_rgb(image))(newInput)
    tmp_out = base_model(x)
    tmpModel = Model(newInput, tmp_out)
    # 出力層を変更
    x = tmpModel.output
    x = GlobalAveragePooling2D()(x)
    predictions = Dense(Y_train.shape[1], activation='softmax')(x)
    model = Model(tmpModel.input, predictions)

sgd = SGD(lr=1e-6, decay=1e-6, momentum=0.9, nesterov=True)
model.compile(optimizer=sgd, loss='categorical_crossentropy', metrics=['accuracy'])
model.load_weights('../data/model/{}_{}_{}_{}x{}.h5'.format(args.dataset, args.nlz, args.model_type, args.im_size, args.im_size))
for layer in model.layers:
    layer.trainable = True

# before train test acc
train_acc_be, train_preds_be = get_preds(model, X_train, Y_train)
test_acc_be, test_preds_be = get_preds(model, X_test, Y_test)
print('before train test acc: {} {}'.format(train_acc_be, test_acc_be))
# before adv train test acc
train_adv_acc_be, train_adv_preds_be = get_preds(model, X_train_adv, Y_train)
test_adv_acc_be, test_adv_preds_be = get_preds(model, X_test_adv, Y_test)
print('before adv train test acc: {} {}'.format(train_adv_acc_be, test_adv_acc_be))
# before adv train test fr
train_fr_be = get_fr(train_adv_preds_be, train_preds_be)
test_fr_be = get_fr(test_adv_preds_be, test_preds_be)
print('before adv train test fr: {} {}'.format(train_fr_be, test_fr_be))

save_model = 'retrained_model/{}_{}_{}_{}x{}_{}_{}_{}_{}.h5'.format(args.dataset, args.nlz, args.model_type, args.im_size, args.im_size, args.target, args.eps, args.norm, args.retrain_type)
cb1 = ModelCheckpoint(save_model, verbose=1, save_weights_only=True)

datagen = ImageDataGenerator()

batch_size = 32 
epoch = 5
if args.dataset == 'melanoma':
    class BalancedDataGenerator(Sequence):
        """ImageDataGenerator + RandomOversampling"""
        def __init__(self, x, y, datagen, batch_size=32):
            self.datagen = datagen
            self.batch_size = batch_size
            self._shape = x.shape        
            datagen.fit(x)
            self.gen, self.steps_per_epoch = balanced_batch_generator(x.reshape(x.shape[0], -1), y, sampler=RandomOverSampler(), batch_size=self.batch_size, keep_sparse=True)

        def __len__(self):
            return self._shape[0] // self.batch_size

        def __getitem__(self, idx):
            x_batch, y_batch = self.gen.__next__()
            x_batch = x_batch.reshape(-1, *self._shape[1:])
            return self.datagen.flow(x_batch, y_batch, batch_size=self.batch_size).next()

    balanced_gen = BalancedDataGenerator(X_re_train, Y_re_train, datagen, batch_size=batch_size)
    steps_per_epoch = balanced_gen.steps_per_epoch

    model.fit_generator(
        balanced_gen,
        steps_per_epoch=steps_per_epoch,
        epochs=epoch,
        validation_data=(X_re_test, Y_re_test),
        callbacks=[cb1],
        verbose=1
    )

else:
    datagen.fit(X_re_train)
    model.fit_generator(datagen.flow(X_re_train, Y_re_train, batch_size=batch_size),
        steps_per_epoch=len(X_re_train) / batch_size, 
        epochs=epoch,
        validation_data=(X_re_test, Y_re_test),
        callbacks=[cb1],
        verbose=1
    )

# after train test acc
train_acc_af, train_preds_af = get_preds(model, X_train, Y_train)
test_acc_af, test_preds_af = get_preds(model, X_test, Y_test)
print('after train test acc: {} {}'.format(train_acc_af, test_acc_af))
# after adv train test acc
train_adv_acc_af, train_adv_preds_af = get_preds(model, X_train_adv, Y_train)
test_adv_acc_af, test_adv_preds_af = get_preds(model, X_test_adv, Y_test)
print('after adv train test acc: {} {}'.format(train_adv_acc_af, test_adv_acc_af))
# after adv train test fr
train_fr_af = get_fr(train_adv_preds_af, train_preds_af)
test_fr_af = get_fr(test_adv_preds_af, test_preds_af)
print('after adv train test fr: {} {}'.format(train_fr_af, test_fr_af))

path_w = 'retrained_model/{}_{}_{}_{}x{}_{}_{}_{}_{}.log'.format(args.dataset, args.nlz, args.model_type, args.im_size, args.im_size, args.target, args.eps, args.norm, args.retrain_type)
with open(path_w, mode='w') as f:
    f.write('retrain_type: {}\n'.format(args.retrain_type))
    f.write('=== before retrain ===\n')
    f.write('train test acc: {} {}\n'.format(train_acc_be, test_acc_be))
    f.write('train test adv acc: {} {}\n'.format(train_adv_acc_be, test_adv_acc_be))
    f.write('train test adv fooling rate: {} {}\n'.format(train_fr_be, test_fr_be))

    f.write('=== after retrain ===\n')
    f.write('train test acc: {} {}\n'.format(train_acc_af, test_acc_af))
    f.write('train test adv acc: {} {}\n'.format(train_adv_acc_af, test_adv_acc_af))
    f.write('train test adv fooling rate: {} {}\n'.format(train_fr_af, test_fr_af))