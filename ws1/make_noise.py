import argparse
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument('--dataset', type=str, default='chestx')
parser.add_argument('--nlz', type=str, default='01') # 11
parser.add_argument('--model_type', type=str, default='inceptionv3')
parser.add_argument('--im_size', type=int)
parser.add_argument('--target', type=int, default=99)
parser.add_argument('--eps', type=float)
parser.add_argument('--norm', type=str, default='2')
parser.add_argument('--dbg', type=bool, default=False)
parser.add_argument('--up_iter', type=int, default=15)
parser.add_argument('--gpu', type=str)
args = parser.parse_args()
print(args)

import tensorflow as tf
import keras
from keras.backend.tensorflow_backend import set_session
config = tf.ConfigProto(
    gpu_options=tf.GPUOptions(
        visible_device_list=args.gpu, # specify GPU number
        allow_growth=True
    )
)
set_session(tf.Session(config=config))

from keras.applications.inception_v3 import InceptionV3
from keras.applications.vgg16 import VGG16
from keras.applications.vgg19 import VGG19
from keras.applications.resnet50 import ResNet50
from keras.layers import Lambda, Input, Dense, GlobalAveragePooling2D
from keras.models import Model

from art.classifiers import KerasClassifier
from art.attacks import UniversalPerturbation

# load data
X_train = np.load('../data/{}/X_train_{}x{}.npy'.format(args.dataset, args.im_size, args.im_size))
Y_train = np.load('../data/{}/Y_train_{}x{}.npy'.format(args.dataset, args.im_size, args.im_size))
if args.dbg:
    X_train, Y_train = X_train[:50], Y_train[:50]

mean_l2_train = 0
mean_inf_train = 0
for im in X_train:
    mean_l2_train += np.linalg.norm(im.flatten(), ord=2)
    mean_inf_train += np.abs(im.flatten()).max()
mean_l2_train /= X_train.shape[0]
mean_inf_train /= X_train.shape[0]

if args.nlz == '01':
    print('nlz: 01')
    X_train /= 255.0
elif args.nlz == '11':
    print('nlz: 11')
    X_train -= 128.0
    X_train /= 128.0

if args.dataset == 'melanoma':
    if args.model_type == 'inceptionv3':
        base_model = InceptionV3(weights='imagenet', input_shape=(args.im_size, args.im_size, 3), include_top=False)
    elif args.model_type == 'vgg16':
        base_model = VGG16(weights='imagenet', input_shape=(args.im_size, args.im_size, 3), include_top=False)
    elif args.model_type == 'vgg19':
        base_model = VGG19(weights='imagenet', input_shape=(args.im_size, args.im_size, 3), include_top=False)
    elif args.model_type == 'resnet50':
        base_model = ResNet50(weights='imagenet', input_shape=(args.im_size, args.im_size, 3), include_top=False)
    x = base_model.output
    x = GlobalAveragePooling2D()(x)
    predictions = Dense(Y_train.shape[1], activation='softmax')(x)
    model = Model(inputs=base_model.input, outputs=predictions)
else:
    # 入力層を変更
    if args.model_type == 'inceptionv3':
        base_model = InceptionV3(weights='imagenet', include_top=False)
    elif args.model_type == 'vgg16':
        base_model = VGG16(weights='imagenet', include_top=False)
    elif args.model_type == 'vgg19':
        base_model = VGG19(weights='imagenet', include_top=False)
    elif args.model_type == 'resnet50':
        base_model = ResNet50(weights='imagenet', include_top=False)
    base_model.layers.pop(0) # remove input layer
    newInput = Input(batch_shape=(None, args.im_size,args.im_size,1))
    x = Lambda(lambda image: tf.image.grayscale_to_rgb(image))(newInput)
    tmp_out = base_model(x)
    tmpModel = Model(newInput, tmp_out)
    # 出力層を変更
    x = tmpModel.output
    x = GlobalAveragePooling2D()(x)
    predictions = Dense(Y_train.shape[1], activation='softmax')(x)
    model = Model(tmpModel.input, predictions)
model.load_weights('../data/model/{}_{}_{}_{}x{}.h5'.format(args.dataset, args.nlz, args.model_type, args.im_size, args.im_size))

# ART
if args.nlz == '01':
    print('ART nlz 01')
    fgsm_eps = 0.0012
else:
    print('ART nlz 11')
    fgsm_eps = 0.0024

if args.norm == '2':
    print('ART norm 2')
    norm = 2
elif args.norm == 'inf':
    print('ART norm inf')
    norm = np.inf

if args.nlz=='01' and args.norm=='2':
    eps = mean_l2_train / 255.0 * args.eps
elif args.nlz=='01' and args.norm=='inf':
    eps = mean_inf_train / 255.0 * args.eps
elif args.nlz=='11' and args.norm=='2':
    eps = mean_l2_train / 128.0 * args.eps
elif args.nlz=='11' and args.norm=='inf':
    eps = mean_inf_train / 128.0 * args.eps
print('eps: {}'.format(eps))

classifier = KerasClassifier(model=model)
if args.target == 99:
    print('nontarget !!!')
    adv_crafter = UniversalPerturbation(
        classifier,
        attacker='fgsm',
        delta=0.000001,
        attacker_params={'targeted':False, 'eps':fgsm_eps},
        max_iter=args.up_iter,
        eps=eps,
        norm=norm)
    
    _ = adv_crafter.generate(X_train, targeted=False)
else:
    print('target {} !!!'.format(args.target))
    adv_crafter = UniversalPerturbation(
        classifier,
        attacker='fgsm',
        delta=0.000001,
        attacker_params={'targeted':True, 'eps':fgsm_eps},
        max_iter=args.up_iter,
        eps=eps,
        norm=norm)
    
    y_train_adv_tar = np.zeros((Y_train.shape[0], Y_train.shape[1]))
    for i in range(Y_train.shape[0]):
        y_train_adv_tar[i, args.target] = 1.0
    
    _ = adv_crafter.generate(X_train, y=y_train_adv_tar, targeted=True)

noise = adv_crafter.noise[0,:]
noise = noise.astype(np.float32)
np.save('noise/{}_{}_{}_{}x{}_{}_{}_{}.npy'.format(args.dataset, args.nlz, args.model_type, args.im_size, args.im_size,
    args.norm, args.target, args.eps), noise)
