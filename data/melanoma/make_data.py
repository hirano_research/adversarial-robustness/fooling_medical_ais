import os 
import argparse
import csv
import random 
import numpy as np
from numpy.random import *
seed = 111
import keras
from keras.preprocessing.image import img_to_array, load_img
from tqdm import tqdm
import time

parser = argparse.ArgumentParser()
parser.add_argument('--image_dir', type=str, default='/root/workspace/dataset/Melanoma/ISIC2018_Task3_Training_Input')
parser.add_argument('--image_gt_csv', type=str, default='/root/workspace/dataset/Melanoma/ISIC2018_Task3_Training_GroundTruth/ISIC2018_Task3_Training_GroundTruth.csv')
parser.add_argument('--im_size', type=int)
args = parser.parse_args()

image_dir = args.image_dir
image_gt_csv = args.image_gt_csv
target_size = (args.im_size, args.im_size)

X = []
Y = []

f = open(image_gt_csv)
reader = csv.reader(f)
header = next(reader)

pbar = tqdm(total=10016)
for i, row in enumerate(reader):
    pbar.update(1)
    image_path = image_dir + '/' + row[0] + '.jpg'
    img = img_to_array(load_img(image_path, grayscale=False, color_mode='rgb', target_size=target_size))
    label = list(map(float, row[1:]))
    X.append(img)
    Y.append(label)
pbar.close()

X = np.asarray(X)
Y = np.asarray(Y)

num_class = Y.shape[1]

Idx = list( range(0, X.shape[0]) )
np.random.seed(seed)
IdxRand = np.random.permutation( Idx )
X, Y = X[IdxRand], Y[IdxRand]

per7 = int(X.shape[0] * 0.7)
X_train, Y_train = X[:per7], Y[:per7]
X_test, Y_test = X[per7:], Y[per7:]

X_train, Y_train = X[:7015], Y[:7015]
X_test, Y_test = X[7015:], Y[7015:]

X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
Y_train = Y_train.astype('float32')
Y_test = Y_test.astype('float32')

print(X_train.shape, Y_train.shape)
print(X_test.shape, Y_test.shape)
print(X_train.dtype, Y_train.dtype)
print(X_test.dtype, Y_test.dtype)

np.save('X_train_{}x{}.npy'.format(args.im_size, args.im_size), X_train)
np.save('X_test_{}x{}.npy'.format(args.im_size, args.im_size), X_test)
np.save('Y_train_{}x{}.npy'.format(args.im_size, args.im_size), Y_train)
np.save('Y_test_{}x{}.npy'.format(args.im_size, args.im_size), Y_test)