import argparse
import numpy as np

parser = argparse.ArgumentParser()
parser.add_argument('--dataset', type=str, default='chestx')
parser.add_argument('--nlz', type=str, default='01') # 11
parser.add_argument('--model_type', type=str, default='inceptionv3')
parser.add_argument('--im_size', type=int)
parser.add_argument('--gpu', type=str)
args = parser.parse_args()
print(args)

import tensorflow as tf
import keras
from keras.backend.tensorflow_backend import set_session
config = tf.ConfigProto(
    gpu_options=tf.GPUOptions(
        visible_device_list=args.gpu, # specify GPU number
        allow_growth=True
    )
)
set_session(tf.Session(config=config))

from keras.applications.inception_v3 import InceptionV3
from keras.applications.vgg16 import VGG16
from keras.applications.vgg19 import VGG19
from keras.applications.resnet50 import ResNet50
from keras.layers import Lambda, Input, Dense, GlobalAveragePooling2D
from keras.models import Model
from keras.callbacks import ModelCheckpoint
from keras.callbacks import LearningRateScheduler
from keras.optimizers import SGD
from keras.optimizers import Adam

from keras.utils.data_utils import Sequence
from imblearn.over_sampling import RandomOverSampler
from imblearn.keras import balanced_batch_generator
from keras.preprocessing.image import ImageDataGenerator

# load data
X_train = np.load('{}/X_train_{}x{}.npy'.format(args.dataset, args.im_size, args.im_size))
X_test = np.load('{}/X_test_{}x{}.npy'.format(args.dataset, args.im_size, args.im_size))
Y_train = np.load('{}/Y_train_{}x{}.npy'.format(args.dataset, args.im_size, args.im_size))
Y_test = np.load('{}/Y_test_{}x{}.npy'.format(args.dataset, args.im_size, args.im_size))
#X_train, X_test, Y_train, Y_test = X_train[:65], X_test[:65], Y_train[:65], Y_test[:65]
if args.nlz == '01':
    print('nlz: 01')
    X_train /= 255.0
    X_test /= 255.0
elif args.nlz == '11':
    print('nlz: 11')
    X_train -= 128.0
    X_test -= 128.0
    X_train /= 128.0
    X_test /= 128.0

if args.dataset == 'melanoma':
    if args.model_type == 'inceptionv3':
        base_model = InceptionV3(weights='imagenet', input_shape=(args.im_size, args.im_size, 3), include_top=False)
    elif args.model_type == 'vgg16':
        base_model = VGG16(weights='imagenet', input_shape=(args.im_size, args.im_size, 3), include_top=False)
    elif args.model_type == 'vgg19':
        base_model = VGG19(weights='imagenet', input_shape=(args.im_size, args.im_size, 3), include_top=False)
    elif args.model_type == 'resnet50':
        base_model = ResNet50(weights='imagenet', input_shape=(args.im_size, args.im_size, 3), include_top=False)
    x = base_model.output
    x = GlobalAveragePooling2D()(x)
    predictions = Dense(Y_train.shape[1], activation='softmax')(x)
    model = Model(inputs=base_model.input, outputs=predictions)
else:
    # 入力層を変更
    if args.model_type == 'inceptionv3':
        base_model = InceptionV3(weights='imagenet', include_top=False)
    elif args.model_type == 'vgg16':
        base_model = VGG16(weights='imagenet', include_top=False)
    elif args.model_type == 'vgg19':
        base_model = VGG19(weights='imagenet', include_top=False)
    elif args.model_type == 'resnet50':
        base_model = ResNet50(weights='imagenet', include_top=False)
    base_model.layers.pop(0) # remove input layer
    newInput = Input(batch_shape=(None, args.im_size,args.im_size,1))
    x = Lambda(lambda image: tf.image.grayscale_to_rgb(image))(newInput)
    tmp_out = base_model(x)
    tmpModel = Model(newInput, tmp_out)
    # 出力層を変更
    x = tmpModel.output
    x = GlobalAveragePooling2D()(x)
    predictions = Dense(Y_train.shape[1], activation='softmax')(x)
    model = Model(tmpModel.input, predictions)

sgd = SGD(lr=1e-3, decay=1e-6, momentum=0.9, nesterov=True)
model.compile(optimizer=sgd, loss='categorical_crossentropy', metrics=['accuracy'])
for layer in model.layers:
    layer.trainable = True

# 学習率
def step_decay(epoch):
    lr = 1e-3
    if epoch > 80:
        lr = 1e-5
    elif epoch > 50:
        lr = 1e-4
    print('Learning rate: ', lr)
    return lr
lr_decay = LearningRateScheduler(step_decay)

save_model = 'model/{}_{}_{}_{}x{}.h5'.format(args.dataset, args.nlz, args.model_type, args.im_size, args.im_size)
cb1 = ModelCheckpoint(save_model, monitor='val_acc', verbose=1,
                     save_best_only=True, save_weights_only=True)

datagen = ImageDataGenerator(
        rotation_range=90,
        width_shift_range=0.1,
        height_shift_range=0.1,
        horizontal_flip=True,
    )

batch_size = 32 
epoch = 100
if args.dataset == 'melanoma':
    class BalancedDataGenerator(Sequence):
        """ImageDataGenerator + RandomOversampling"""
        def __init__(self, x, y, datagen, batch_size=32):
            self.datagen = datagen
            self.batch_size = batch_size
            self._shape = x.shape        
            datagen.fit(x)
            self.gen, self.steps_per_epoch = balanced_batch_generator(x.reshape(x.shape[0], -1), y, sampler=RandomOverSampler(), batch_size=self.batch_size, keep_sparse=True)

        def __len__(self):
            return self._shape[0] // self.batch_size

        def __getitem__(self, idx):
            x_batch, y_batch = self.gen.__next__()
            x_batch = x_batch.reshape(-1, *self._shape[1:])
            return self.datagen.flow(x_batch, y_batch, batch_size=self.batch_size).next()

    balanced_gen = BalancedDataGenerator(X_train, Y_train, datagen, batch_size=batch_size)
    steps_per_epoch = balanced_gen.steps_per_epoch

    model.fit_generator(
        balanced_gen,
        steps_per_epoch=steps_per_epoch,
        epochs=epoch,
        validation_data=(X_test, Y_test),
        callbacks=[lr_decay, cb1],
        verbose=1
    )

else:
    datagen.fit(X_train)
    model.fit_generator(datagen.flow(X_train, Y_train, batch_size=batch_size),
        steps_per_epoch=len(X_train) / batch_size, 
        epochs=epoch,
        validation_data=(X_test, Y_test),
        callbacks=[lr_decay, cb1],
        verbose=1
    )

